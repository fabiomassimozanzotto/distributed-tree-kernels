========
Distributed Tree Kernels (DTK)

(c) Fabio Massimo Zanzotto & Lorenzo Dell'Arciprete
========

This repository contains:
- the DTK maven project
- two sample files: SampleInput.dat and SampleOutput.dat

Software facts and instrutions
-------
- DTK is java package in a maven project 
- To compile it,  use Eclipse (or other maven-enabled IDEs) 
  or by directly executing the maven command:
       mvn clean compile assembly:single
	   
- To use it as a command to produce Distributed Trees,  the 
  class to use it.uniroma2.dtk.main.DTBuilder
  Then, execute:
      java -cp target/DTK-[VersionNumber].jar it.uniroma2.dtk.main.DTBuilder
  to obtain the current list of parameters
-------       
For testing the software, run:

	java -cp target/DTK-[VersionNumber].jar it.uniroma2.dtk.main.DTBuilder -input SampleInput.dat -output SampleOutputNew.dat -op it.uniroma2.dtk.op.convolution.ShuffledCircularConvolution -vectorSize 4096

and check that SampleOutputNew.dat and SampleOutput.dat are not different.
-------       

Refer to:

  Fabio Massimo Zanzotto and Lorenzo Dell'Arciprete, Distributed Tree Kernels, Proceedings of ICML, 2012 

for further information.