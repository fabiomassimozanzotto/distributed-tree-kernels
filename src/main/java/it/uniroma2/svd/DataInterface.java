package it.uniroma2.svd;

import java.io.IOException;
import java.util.TreeMap;


public interface DataInterface {
        public static final String tf_file = "tf_map";
        public static final String idf_file = "idf_map";
        public static final String word_file = "words_id";
        public static final String doc_file = "docs_id";
        
        /**
         * Funzione che aggiunge un documento
         * @param doc Nome del documento da aggiungere
         * @param numberWords Numero di termini presenti nel documento. Serve per ottenere una normalizzazione dei termini rispetto al numero totale dei termini presenti nel documento. 
         * @return id numerico assegnato al documento o null in caso di errore
         */
        public Integer addDoc(String doc, Integer numberWords);
        
        
        /**
         * Funzione che aggiunge un termine a quelli gia' contenuti in memoria
         * @param term termine da aggiungere
         * @param idDoc id numerico del documento
         * @return <b>true</b> in caso di successo <b>false</b> altrimenti 
         */
        public Boolean addTerm(String term, Integer idDoc);
        
        /**
         * Funzione che aggiunge un termine a quelli gia' contenuti in memoria, con una cardinalita' maggiore di uno
         * @param term termine da aggiungere
         * @param numero di occorrenze del termine
         * @param idDoc id numerico del documento
         * @return <b>true</b> in caso di successo <b>false</b> altrimenti 
         */
        public Boolean addTerm(String term, Integer count, Integer idDoc);
        
        /**
         * Funzione che restituisce il valore delle occorrenze di un termine in un documento.
         *  Questo valore risultera' gia' normalizzato secondo la metrica di pesatura scelta
         * @param term termine
         * @param Doc id numerico del documento
         * @return il valore espresso cime float
         */
        //public Float getTermDocValue(String term, Integer Doc);
        
        /**
         * Funzione che restituisce il valore delle occorrenze di un termine in un documento.
         * Questo valore risultera' gia' normalizzato secondo la metrica di pesatura scelta
         * @param term termine
         * @param Doc nome del documento
         * @return il valore espresso cime float
         */
        //public Float getTermDocValue(String term, String Doc);
        
        /**
         * Funzione che restituisce tutti i valori non nulli dei pesi associati ad uno specifico termine.
         * Ciascun valore risultera' gia' normalizzato secondo la metrica di pesatura scelta
         * @param term termine
         * 
         * @return una mappa ordinata di valiori che usa come chiave l'id numerico del documento
         */
        //public TreeMap<Integer, Float> getTermValues(String term);
        
        
        /**
         * Funzione che restituisce tutti i valori non nulli dei pesi associati ad uno specifico documento.
         * Ciascun valore risultera' gia' normalizzato secondo la metrica di pesatura scelta
         * @param idDoc id numerico del documento
         * 
         * @return una mappa non ordinata di valiori che usa come chiave una stringa che rappresenta il termine
         */
        //public HashMap<String, Float> getDocValuesWithIntKey(Integer idDoc);
        /**
         * Funzione che restituisce tutti i valori non nulli dei pesi associati ad uno specifico documento.
         * Ciascun valore risultera' gia' normalizzato secondo la metrica di pesatura scelta
         * @param Doc Stringa che rappresenta il nome con cui e' stato indicizzato il documento
         * 
         * @return una mappa non ordinata di valiori che usa come chiave una stringa che rappresenta il termine
         */
        //public HashMap<String, Float> getDocValuesWithStringKey(String Doc);
        /**
         * Funzione che restituisce tutti i valori non nulli dei pesi associati ad uno specifico documento.
         * Ciascun valore risultera' gia' normalizzato secondo la metrica di pesatura scelta
         * @param Doc Stringa che rappresenta il nome con cui e' stato indicizzato il documento
         * 
         * @return una mappa non ordinata di valiori che usa come chiave una stringa che rappresenta il termine
         */
        //public HashMap<String, Float> getDocValuesWithIntKey(String Doc);
        /**
         * Funzione che retituisce il numero di termini indicizzati
         * 
         * @return numero di termini indicizzati
         */
        public Integer termSize();
        
        /**
         * Funzione che retituisce il numero di documenti
         * 
         * @return numero di documenti indicizzati
         */
        public Integer documentSize();
        
        /**
         * Funzione esegue il salvataggio su disco dei dati in memoria
         * 
         * @return true se i dati sono stati salvati correttamente false altrimenti
         */
        public void save() throws IOException;
        
        /**
         * Funzione esegue il caricamento dei dati in memoria
         * 
         * @return true se i dati sono stati caricati correttamente false altrimenti
         */
        public Boolean load();
        
        /**
         * funzione che restituisce un valore medio associato al termine
         * 
         * @param word Termine per cui e' richiesto il valore
         * @return il valore richiesto
         */
        public Double getWordWeight(String word );
        
        /**
         * funzione che restituisce un valore medio associato al termine
         * 
         * @param word_id identificativo del termine per cui e' richiesto il valore
         * @return il valore richiesto
         */
        public Double getWordWeight(Integer word_id );
        
        /**
         * funzione che restituisce l'identificativo di un termine
         * 
         * @param word Termine per cui e' richiesto l'id
         * @return l'id del termine
         */
        public Integer getWordId(String word) throws Exception; 
        
        /**
         * funzione che restituisce l'identificativo di un documento
         * 
         * @param doc Docuemnto per cui e' richiesto l'id
         * @return l'id del documento
         */
        public Integer getDocId(String doc) throws Exception; 
        
        /**
         * funzione che restituisce la stringa rappresentativa di un termine
         * 
         * @param id Id del termine
         * @return Stringa rappresentativa del termine
         */
        public String getWord(Integer id);
        
        /**
         * funzione che restituisce la stringa rappresentativa di un documento
         * 
         * @param id Id del documento
         * @return Stringa rappresentativa del documento
         */
        public String getDoc(Integer id);
        
        /**
         * Funzione che restituisce il valore di un termine dato un documento secondo la pesatura implementata
         * @param idword id del termine 
         * @param iddoc id del documento
         * @return valere secondo la pesatura implementata
         */
        public Float getValue(Integer idword,Integer iddoc);
        
        /**
         * Funzione che restituisce l'insieme dei singoli valori dei termini presenti in un documento. <br> L'isieme e' costitutito da una mappa ordinata secondo gli id dei permini presenti nel documento
         * @param iddoc id del documento
         * @return Mappa ordinata sencondo gli id dei termini
         */
        public TreeMap<Integer,Float> getDocValues(Integer iddoc);
}