package it.uniroma2.svd.writer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

public class ReadFileWords {
	
	private Vector<String> frame;
	private Vector<String> words;
	private Vector<String> pos;
	
	private Vector<Integer> TypeFrame= new Vector<Integer>();
	private Vector<String> LU1= new Vector<String>();
	private Vector<String> LU2= new Vector<String>();

	public ReadFileWords() throws Exception{
		
	}
		

	
	public ReadFileWords(String path) throws Exception{
		
		words = new Vector<String>();
		frame = new Vector<String>();
		pos = new Vector<String>();
		
		try {
			File _file = new File(path);
			if(!_file.isFile() || !_file.canRead()){
				return;
			}
			FileReader fr = new FileReader(_file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			StringTokenizer st;
			while ((line=br.readLine())!=null) {
				line = line.trim();
				if(line.length() == 0)
					continue;
				
				st = new StringTokenizer(line,"\t");
				frame.addElement(st.nextToken().trim());
				words.addElement(st.nextToken().trim());
				pos.addElement(st.nextToken().trim());


			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public void readFileLUs(String path) throws Exception{
		
		try {
			File _file = new File(path);
			if(!_file.isFile() || !_file.canRead()){
				return;
			}
			FileReader fr = new FileReader(_file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			StringTokenizer st;
			while ((line=br.readLine())!=null) {
				line = line.trim();
				if(line.length() == 0)
					continue;
				
				st = new StringTokenizer(line,"\t");
				
				TypeFrame.addElement(Integer.parseInt(st.nextToken().trim()));
				LU1.addElement(st.nextToken().trim());
				LU2.addElement(st.nextToken().trim());
				
				//System.out.println(LU1.lastElement()+" "+LU2.lastElement());


			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	
	public Vector<String> getWords(){
		return words;
	}
	public Vector<String> getFrame(){
		return frame;
	}
	public Vector<String> getPOS(){
		return pos;
	}

	/**
	 * @return the typeFrame
	 */
	public Vector<Integer> getTypeFrame() {
		return TypeFrame;
	}

	/**
	 * @param typeFrame the typeFrame to set
	 */
	public void setTypeFrame(Vector<Integer> typeFrame) {
		TypeFrame = typeFrame;
	}

	/**
	 * @return the lU1
	 */
	public Vector<String> getLU1() {
		return LU1;
	}

	/**
	 * @param lu1 the lU1 to set
	 */
	public void setLU1(Vector<String> lu1) {
		LU1 = lu1;
	}

	/**
	 * @return the lU2
	 */
	public Vector<String> getLU2() {
		return LU2;
	}

	/**
	 * @param lu2 the lU2 to set
	 */
	public void setLU2(Vector<String> lu2) {
		LU2 = lu2;
	}
	
}
