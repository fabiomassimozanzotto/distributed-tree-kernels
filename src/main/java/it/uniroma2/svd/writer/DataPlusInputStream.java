package it.uniroma2.svd.writer;

import java.io.*;


public class DataPlusInputStream extends RandomAccessFile{
	RandomAccessFile in;

    /**
     * Constructor
     * @param is An input stream that this is chained to.
     * @throws FileNotFoundException 
     */
    public DataPlusInputStream(File in) throws FileNotFoundException {
    	super(in,"rw");
    }
    
	
    public String readString(int length)  throws IOException  {
    	byte[] array = new byte[length];
    	readFully(array);
    	String s = new String(array);
    	return s;
        }

}
