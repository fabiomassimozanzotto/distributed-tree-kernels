package it.uniroma2.svd.writer;

import java.io.*;

public class DataPlusOutputStream extends DataOutputStream
{

    protected int written;

    public DataPlusOutputStream(OutputStream out)
    {
        super(out);
    }


    public void writeString(String string, int length)
        throws IOException
    {
        if(string.length() < length)
        {
            String newstring = zeroFill(string, length);
            byte bytes[] = newstring.getBytes();
            out.write(bytes);
        } else
        {
            String newstring = string.substring(0, length);
            byte bytes[] = newstring.getBytes();
            out.write(bytes);
        }
    }

    private String zeroFill(String string, int length)
    {
        char oldchars[] = string.toCharArray();
        char newchars[] = new char[length];
        for(int i = 0; i <= newchars.length - 1; i++)
            if(i <= oldchars.length - 1)
                newchars[i] = oldchars[i];
            else
                newchars[i] = '\0';

        return new String(newchars);
    }
}

