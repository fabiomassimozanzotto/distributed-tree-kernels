package it.uniroma2.svd.writer;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class ReadFileWeigth {
	private Vector<Float> weight;
	public ReadFileWeigth(String path){
		weight = new Vector<Float>();
		try {
			File _file = new File(path);
			if(!_file.isFile() || !_file.canRead()){
				return;
			}
			FileReader fr = new FileReader(_file);
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			while ((line=br.readLine())!=null) {
				line = line.trim();
				if(line.length() == 0)
					continue;
				weight.add(Float.valueOf(line));
			}
			br.close();
			fr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	public Vector<Float> getWeight(){
		return weight;
	}
}
