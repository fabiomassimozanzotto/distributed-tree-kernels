package it.uniroma2.svd.writer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Classe di lettura matrice dei valori LSA e altre propieta' statistiche
 * (parola, TF, DF, vettore LSA)
 * 
 * @author oem
 *
 */
public class ReadFileLSA   {
	
	
	private Vector<String> info = new Vector<String>();
	private Vector<Float> TF = new Vector<Float>();
	private Vector<Float> DF = new Vector<Float>();
	private Vector <Object>data = new Vector<Object>();

	
	/**
	 * 
	 * @param filename Nome file del quale leggere le linee
	 * @param delim	Delimitatore token della linea
	 * @return num linee file
	 * 
	 * @throws Exception
	 */

	public  ReadFileLSA (String filename, String delim) throws Exception   {
		
	
		try {
		
												
//					FileReader infilereader = new FileReader(filename);   // piu lento				
					InputStreamReader infilereader = new InputStreamReader(new FileInputStream(filename));
					BufferedReader in = new BufferedReader(infilereader);			

					
					
					StringTokenizer st;
					String line = in.readLine();	
					
					// formato linea word\tTF\tDF\tW1,W2,...,WN\n
					
					while (line != null){
					
						if (line.length() != 0)	{	// salta le linee vuote
							
							st = new StringTokenizer(line,delim);
							if (st.countTokens() != 4) {
								System.out.print("linea scritta male: numero di campi diversi da quelli previsti");
								throw new Exception ("linea scritta male: numero di campi diversi da quelli previsti");
							}

							info.addElement(st.nextToken().trim());
							TF.addElement(Float.valueOf(st.nextToken().trim()));
							DF.addElement(Float.valueOf(st.nextToken().trim()));
							data.addElement(st.nextToken().trim());
							
						} 
						
						line = in.readLine();

					}			
					
					
					in.close();	
					infilereader.close();
		
		} catch ( Exception e ) {  
			throw e;
		} 
		
		
	}

	public Vector<Object>  getData() {
		return data;
	}

	public Vector<Float>  getDF() {
		return DF;
	}


	public Vector<String> getInfo() {
		return info;
	}


	public Vector<Float>  getTF() {
		return TF;
	}



	
}
