package it.uniroma2.svd.writer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.TreeMap;

public interface BinaryMatrix<T> {
	public void openFile(String file, String mode) throws IOException;
	public void closeFile() throws IOException;
	public int getCols();
	public int getRows();
	
	public T getElement(int row, int col) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	public T[] getFullRow(int row) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException;
	public T[] getFullCol(int col) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	public TreeMap<Integer, T> getSparseRow(int row) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException;
	public TreeMap<Integer, T> getSparseCol(int col) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	
	public void setElement(int row, int col, T value) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	public void setFullRow(int row, T[] values) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	public void setFullCol(int col, T[] colelem) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	public void setSparseRow(int row, Map<Integer, T> values) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	public void setSparseCol(int col, Map<Integer, T> values) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
//	public void setSparse(SortedMap<Integer, T> items) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException;
	
}
