package it.uniroma2.svd.writer;


import it.uniroma2.svd.writer.FileTools;
import it.uniroma2.svd.DataInterface;

public interface FileTools {
	public enum type {DOC,WORD}
	
	public void initHandle  (String PathOutputW) throws  Exception;
	
	public void closeHandle () throws  Exception ;
		
	
	public Object[] loadToken (String PathInput)  throws  Exception;
	
	

	public void writeTokenLine 	(
			float[]matrixDataRow, 
			int index,
			DataInterface data,
			FileTools.type type)  throws  Exception;
	
	
	public void writeToken (
			float[][]matrixDataRow, 
			String pathOutput,
			DataInterface data,
			FileTools.type type)  throws  Exception;
	

}
