package it.uniroma2.exp.tools;

import it.uniroma2.util.tree.Tree;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TreeFlattenerForSK {
	
	public static void main(String [] argv) throws Exception {
		if (argv.length < 2) {
			System.out.println("Provide a destination folder and at least one file to process!");
		}
		File destFolder = new File(argv[0]);
		if (!destFolder.exists())
			destFolder.mkdirs();
		for (int i=1; i<argv.length; i++) {
			File input = new File(argv[i]); 
			process(input, new File(destFolder, input.getName()));
		}
	}
	
	public static void process(File input, File output) throws Exception {
		System.out.println("Processing "+input.getPath());
		BufferedReader in = new BufferedReader(new FileReader(input));
		BufferedWriter out = new BufferedWriter(new FileWriter(output));
		String line = in.readLine();
		while (line != null) {
			StringBuilder newLine = new StringBuilder();
			boolean more = false;
			int lastEnd = 0;
			int nextStart = line.indexOf("|BT|", lastEnd);
			if (nextStart > 0) {
				newLine.append(line.substring(lastEnd, nextStart));
				more = true;
			}
			while (more) {
				newLine.append("|BT|");
				nextStart = nextStart + 4;
				lastEnd = line.indexOf("|BT|", nextStart);
				if (lastEnd == -1) {
					lastEnd = line.indexOf("|ET|", nextStart);
					if (lastEnd == -1) {
						System.out.println("Malformed tree: "+line);
						break;
					}
					else 
						more = false;
				}
				String tree = line.substring(nextStart, lastEnd).trim(); 
				String flatTree = buildFlatTree(flattenTree(Tree.fromPennTree(tree)));
				newLine.append(" ").append(flatTree).append(" ");
				nextStart = lastEnd;
			}
			newLine.append(line.substring(lastEnd));
			out.write(newLine.toString());
			out.newLine();
			line = in.readLine();
		}
		out.close();
		in.close();
	}
	
	public static List<String> flattenTree(Tree tree) {
		if (tree.isTerminal())
			return Collections.singletonList(tree.getRootLabel());
		else {
			List<String> tokens = new ArrayList<String>();
			for (Tree child : tree.getChildren())
				tokens.addAll(flattenTree(child));
			return tokens;
		}
	}
	
	public static String buildFlatTree(List<String> tokens) {
		StringBuilder sb = new StringBuilder();
		sb.append("(S ");
		for (String token : tokens) {
			sb.append("(");
			sb.append(token);
			sb.append(" x)");
		}
		sb.append(")");
		return sb.toString();
	}
	
	public static String cleanTree(String tree) {
		int countPar = 0;
		for (int i=0; i<tree.length(); i++) {
			if (tree.charAt(i)=='(')
				countPar++;
			else if (tree.charAt(i)==')')
				countPar--;
		}
		if (countPar == -1)
			return tree.substring(0, tree.length()-1);
		else 
			return tree;
	}

}
