package it.uniroma2.exp.tools;

public class AvgVarCalculator {
	
	private int samplesNum;
	private double sampleSum;
	private double sampleSquareSum;
	
	private Double max;
	private Double min;
	
	private double lastSample;
	
	public AvgVarCalculator() {
		reset();
	}
	
	public void reset() {
		samplesNum = 0;
		sampleSum = 0;
		sampleSquareSum = 0;
		lastSample = 0;
	}
	
	public void addSample(double sample) {
		if (max == null || sample > max)
			max = sample;
		if (min == null || sample < min)
			min = sample;
		samplesNum++;
		sampleSum += sample;
		sampleSquareSum += sample*sample;
		lastSample = sample;
	}
	
	public double getAvg() {
		return sampleSum/samplesNum;
	}
	
	public double getVar() {
		return sampleSquareSum/samplesNum - getAvg()*getAvg();
	}
	
	public double getLastSample() {
		return lastSample;
	}
	
	public Double getMax() {
		return max;
	}

	public Double getMin() {
		return min;
	}
	
	public String getFormattedResult() {
		return getFormattedResult(4);
	}
	
	public String getFormattedResult(int precision) {
		return String.format("%."+precision+"f\t%."+precision+"f", getAvg(), getVar());
	}

}
