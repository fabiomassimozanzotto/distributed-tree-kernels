package it.uniroma2.exp.dtk;

import it.uniroma2.exp.AbstractExperiment;
import it.uniroma2.exp.tools.AvgVarCalculator;
import it.uniroma2.tk.TreeKernel;
import it.uniroma2.util.math.ArrayMath;
import it.uniroma2.util.tree.RandomTreeGenerator;
import it.uniroma2.util.tree.Tree;
import it.uniroma2.util.vector.RandomVectorGenerator;

public class TimeTester extends AbstractExperiment {

	public static final int TREES_PER_SIZE = 10;
	public static final int LABELS = 15;
	public static final int DEGREE = 5;
	public static final int MAX_NODES = 200;
	public static final int[] VECTOR_SIZES = new int[] {1024, 2048, 4096, 8192};
	
	@Override
	protected void runExperiment() throws Exception {
		AvgVarCalculator time = new AvgVarCalculator();
		out.println("TK times");
		RandomTreeGenerator rtg = new RandomTreeGenerator(randomOffset);
		for (int nodes=2; nodes<MAX_NODES; nodes++) {
			time.reset();
			for (int j=0; j<TREES_PER_SIZE; j++) {
				Tree t1 = Tree.fromPennTree(rtg.generateRandomTree(LABELS, DEGREE, nodes));
				Tree t2 = Tree.fromPennTree(rtg.generateRandomTree(LABELS, DEGREE, nodes));
				System.gc();
				long start = System.currentTimeMillis();
				TreeKernel.value(t1, t2);
				time.addSample(System.currentTimeMillis()-start);
			}
			out.println(nodes+"\t"+time.getAvg());
		}
		out.println("DTK times");
		for (int size : VECTOR_SIZES) {
			time.reset();
			RandomVectorGenerator rvg = new RandomVectorGenerator(size, randomOffset);
			for (int i=0; i<MAX_NODES*TREES_PER_SIZE; i++) {
				double[] v1 = rvg.generateRandomVector();
				double[] v2 = rvg.generateRandomVector();
				long start = System.currentTimeMillis();
				ArrayMath.dot(v1, v2);
				time.addSample(System.currentTimeMillis()-start);
			}
			out.println(size+"\t"+time.getAvg());
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TimeTester tt = new TimeTester();
		tt.setRandomOffset(0);
		tt.runAll();
	}

}
