package it.uniroma2.exp.dtk;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import it.uniroma2.exp.AbstractExperiment;
import it.uniroma2.exp.tools.AvgVarCalculator;
import it.uniroma2.util.math.ArrayMath;
import it.uniroma2.util.math.MatrixUtils;
import it.uniroma2.util.vector.RandomVectorGenerator;
import it.uniroma2.util.vector.VectorComposer;

/**
 * @author Lorenzo Dell'Arciprete
 * 
 * This experiment computes the dot product between 2 sums of N random vectors, where M of them are in common
 *
 */
public class VectorTester extends AbstractExperiment {
	
	public static final int[] NUMBER_OF_VECTORS = {20, 50, 100, 200, 500};
	public static final int[] NUMBER_OF_COMMON_VECTORS = {0, 1, 2, 5, 10}; 
	public static final int TRIALS = 100;
	
	public static void main(String[] args) {
		VectorTester vt = new VectorTester();
		try {
			vt.setOutputStream(new PrintStream(new File("vector_tester.dat")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		}
		vt.setVectorSizeArray(new int[] {4096});
		vt.runAll();
	}

	@Override
	protected void runExperiment() throws Exception {
		RandomVectorGenerator rvg = new RandomVectorGenerator(vectorSize, randomOffset);
		out.print("\t");
		for (int commonVecs : NUMBER_OF_COMMON_VECTORS)
			out.print(commonVecs+"\t\t");
		for (int numVecs : NUMBER_OF_VECTORS) {
			rvg.setRandomSeed(vectorSize*numVecs);
			out.print("\n"+numVecs+"\t");
			for (int commonVecs : NUMBER_OF_COMMON_VECTORS) {
				AvgVarCalculator avg = new AvgVarCalculator();
				for (int k = 0; k < TRIALS; k++) {
					double[] sum1 = MatrixUtils.uniformVector(rvg.getVectorSize(), 0);
					double[] sum2 = MatrixUtils.uniformVector(rvg.getVectorSize(), 0);
					for (int i = 0; i < commonVecs; i++) {
						double[] vec = rvg.generateRandomVector();
						sum1 = VectorComposer.sum(sum1, vec);
						sum2 = VectorComposer.sum(sum2, vec);
					}
					for (int i = 0; i < numVecs - commonVecs; i++)
						sum1 = VectorComposer.sum(sum1, rvg.generateRandomVector());
					for (int i = 0; i < numVecs - commonVecs; i++)
						sum2 = VectorComposer.sum(sum2, rvg.generateRandomVector());
					avg.addSample(ArrayMath.dot(sum1, sum2));
				}
				out.print(avg.getFormattedResult()+"\t");
			}
		}
		out.println();
	}

}
