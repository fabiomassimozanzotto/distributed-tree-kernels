package it.uniroma2.util.vector;

/**
 * @author Lorenzo Dell'Arciprete
 * Interface that allows decoupling of the specific node vectors fetching procedure and the DTK algorithm
 */
public interface VectorProvider {
	
	/**
	 * Returns the random vector associated with the given node label
	 */
	public double[] getVector(String term) throws Exception;
	
	public int getVectorSize();

}
