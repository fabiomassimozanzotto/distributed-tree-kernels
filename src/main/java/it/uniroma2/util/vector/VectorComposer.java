package it.uniroma2.util.vector;


import java.util.Arrays;


/**
 * @author Lorenzo Dell'Arciprete
 * This class provides some vector composition or trasnformation operations.
 */
public class VectorComposer {
	
	public static double[] sum(double[] firstVector, double[] secondVector) {
		if (firstVector == null)
			return secondVector;
		else if (secondVector == null)
			return firstVector;
		
		int size = firstVector.length;
		double[] result = new double[size];
		for (int i=0; i<size; i++)
			result[i] = firstVector[i] + secondVector[i];
		return result;
		/*
		DenseMatrix64F f = DenseMatrix64F.wrap(firstVector.length,1,firstVector);
		DenseMatrix64F s = DenseMatrix64F.wrap(secondVector.length,1,secondVector);
		DenseMatrix64F out = new DenseMatrix64F(firstVector.length,1);
		CommonOps.add(f, s, out);
		return out.getData();
		*/
		
	}
	
	public static double[] reverse(double[] vector) {
		if (vector == null)
			return null;
		int size = vector.length;
		double[] result = new double[size];
		for (int i=0; i<size; i++)
			result[i] = vector[vector.length-1-i];
		return result;
	}
	
	public static double[] shift(double[] vector) {
		if (vector == null)
			return null;
		int size = vector.length;
		double[] result = new double[size];
		result[0] = vector[size-1];
		for (int i=1; i<size; i++)
			result[i] = vector[i-1];
		return result;
	}

	public static double[] shuffle(double[] vector, int[] permutation) {
		if (vector == null || vector.length != permutation.length)
			return null;
		double[] shuffled = new double[vector.length];
		for (int i=0; i<vector.length; i++)
			if (permutation[i] >= 0)
				shuffled[i] = vector[permutation[i]];
			else
				shuffled[i] = -vector[-permutation[i]];
		return shuffled;
	}
	
	public double[] matrixTransform(double[] vector, double[][] matrix) {
		if (vector == null || vector.length != matrix.length || vector.length != matrix[0].length)
			return null;
		int size = vector.length;
		double[] translated = new double[size];
		Arrays.fill(translated, 0);
		double[] secondTranslated = new double[size];
		Arrays.fill(secondTranslated, 0);
		for (int i=0; i<size; i++)
			for (int j=0; j<size; j++)
				translated[i] += vector[j] * matrix[i][j];
		return translated;
	}
	
}
