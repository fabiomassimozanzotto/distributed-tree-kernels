package it.uniroma2.util.vector;

import it.uniroma2.util.math.ArrayMath;

import java.util.Hashtable;
import java.util.Random;
import java.util.Vector;



/**
 * @author Lorenzo Dell'Arciprete
 * @author Fabio Massimo Zanzotto
 * This class generates random vectors for the tree node labels by using the label String hashcode
 * as the seed for the Random generator used to fetch the vector elements. This is used in conjunction
 * with a secondary seed that acts as a common offset.
 */
public class RandomVectorGenerator implements VectorProvider {
	private Random random;
	private int offset;
	private int vectorSize;
	private Hashtable <String,double[]> cache;
	private Hashtable <String,Integer> cacheUsage;
	private double variance = 0;
	private boolean stopCaching = false;
	

	public RandomVectorGenerator(int vectorSize) {
		this(vectorSize, 0);
	}
	
	public RandomVectorGenerator(int vectorSize, int offset) {
		random = new Random(offset);
		this.offset = offset;
		this.vectorSize = vectorSize;
		cache = new Hashtable <String,double[]> ();
		cacheUsage = new Hashtable <String,Integer> ();
		variance = 1/Math.sqrt((double)vectorSize);
	}
	
	public void setVectorSize(int vectorSize) {
		this.vectorSize = vectorSize;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public void setRandomSeed(int seed) {
		random.setSeed(seed+offset);
	}
	
	public boolean isStopCaching() {
		return stopCaching;
	}

	public void setStopCaching(boolean stopCaching) {
		this.stopCaching = stopCaching;
	}
	
	public double[] generateRandomVector() { 
		/* OLD VERSION*/
		double[] randomVector = new double[vectorSize];
		for (int i=0; i<vectorSize; i++)
			randomVector[i] = random.nextGaussian();
		try {
			randomVector = ArrayMath.versor(randomVector);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return randomVector;
		
		/*
		DenseMatrix64F v = RandomMatrices.createRandom(vectorSize, 1, random);
		NormOps.normalizeF(v);
		return v.getData();
		CovarianceRandomDraw crd = new CovarianceRandomDraw(random, cov);
		//DenseMatrix64F v = RandomMatrices.createRandom(1, vectorSize, random);
		DenseMatrix64F v = new DenseMatrix64F(vectorSize,1);
		crd.next(v);
		return v.getData();
		*/		
	}

	
//	public DenseMatrix64F generateRandomVectorDenseMatrix64F(int seed) { 
//		setRandomSeed(seed);
//		return RandomMatrices.createGaussian(1, vectorSize , 0 , variance, random);
//	} 

	public double[] generateRandomVector(int seed) {
		setRandomSeed(seed);
		return generateRandomVector();
	}

	public double[] getVector(String term) {
		// THIS CACHING DOES NOT CONTROL THE MEMORY OCCUPATION.
		double[] out = null;
		if (cache.containsKey(term)) {
			out = cache.get(term);
			cacheUsage.put(term, cacheUsage.get(term)+1);
		} else {
			out = generateRandomVector(term.hashCode());
			if (!stopCaching) {
				cache.put(term, out);
				cacheUsage.put(term, 0);
			}
		}
		return out;
//		return generateRandomVector(term.hashCode());
	}

	public int getVectorSize() {
		return vectorSize;
	}
	
	
	public void flushCache(int trh) {
		System.out.print("\nFreeing base vector memory");
		int removed = 0;
		Vector <String> toRemove = new Vector<String>();

		for (String k:cacheUsage.keySet()) {
			if (cacheUsage.get(k) < trh) { toRemove.add(k);}
		}
		for (String k:toRemove) {cache.remove(k); cacheUsage.remove(k); removed++;}
		System.out.println(": removed " + removed  + " vectors");
	}
}
