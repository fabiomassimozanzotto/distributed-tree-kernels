package it.uniroma2.util.math.statistical;

public class RankSample {
	//fields
	int index;
	float rank;
	float value;
	
	public RankSample(int index, float value){
		this.index = index;
		this.value = value;
	}
}
