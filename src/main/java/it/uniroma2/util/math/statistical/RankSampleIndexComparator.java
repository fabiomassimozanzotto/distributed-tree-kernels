package it.uniroma2.util.math.statistical;

import java.util.Comparator;

/**Sorts by index, small to large.*/
public class RankSampleIndexComparator implements Comparator<Object> {
	public int compare(Object arg0, Object arg1) {
		RankSample first = (RankSample)arg0;
		RankSample second = (RankSample)arg1;
		if (second.index < first.index) return 1;
		if (second.index > first.index) return -1;
		return 0;
	}
	
}
