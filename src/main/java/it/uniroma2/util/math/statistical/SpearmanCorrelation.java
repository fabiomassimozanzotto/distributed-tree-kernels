package it.uniroma2.util.math.statistical;

import java.io.*;
import java.util.*;

//import de.pado.sigf.*;

/**Calculates a Spearman correlation coefficient (rho), in the case of ties, averages the ranks 
 * and returns a Pearson correlation coefficient on the ranks.
 * @author Nix
 */
public class SpearmanCorrelation {
	
	//fields
	private RankSampleIndexComparator indexComp;
	private RankSampleValueComparator valueComp;
	private RankedFloatArray aRFA;
	private RankedFloatArray bRFA;
	
	//constructor
	public SpearmanCorrelation(){
		indexComp = new RankSampleIndexComparator();
		valueComp = new RankSampleValueComparator();
	}
	
	/**Returns a Spearman Rank Correlation Coefficient (rho) between the 
	 * two arrays.  If ties are found, their ranks are averaged and a 
	 * Pearson correlation is calculated on the ranks.*/
	public double spearmanCorrelationCoefficient (float[] a, float[] b){
		aRFA = rank (a);
		bRFA = rank (b);
		if (aRFA.tiesFound || bRFA.tiesFound) return PearsonCorrelation.correlationCoefficient(aRFA.ranks, bRFA.ranks);
		else return corrCoeff (aRFA.ranks, bRFA.ranks);
	}
	
	/**Returns a Spearman Rank Correlation Coefficient (rho) between the 
	 * two arrays.  If ties are found, their ranks are averaged and a 
	 * Pearson correlation is calculated on the ranks.*/
	public double spearmanCorrelationCoefficient (RankedFloatArray aRFA, RankedFloatArray bRFA){
		if (aRFA.tiesFound || bRFA.tiesFound) return PearsonCorrelation.correlationCoefficient(aRFA.ranks, bRFA.ranks);
		else return corrCoeff (aRFA.ranks, bRFA.ranks);
	}
	
	/**This is the final step in calculating a Spearman Correlation coeff.
	 * Don't use it unless you know what you are doing
	 * @param a - a values converted to ranked and sorted by original index
	 * @param b - ditto
	 * Call correlate() if you don't have the ranks.*/
	public double corrCoeff (float[] a, float[] b){
		//calculate sum of square differences
		double sumSqrDiffs = 0;
		for (int i=0; i< a.length; i++) {
			double diff = a[i]-b[i];
			sumSqrDiffs += (diff * diff);
		}
		//numerator 6(sumSqrDiffs)
		double numer = 6* sumSqrDiffs;
		//denominator n(n^2-1)
		double n = a.length;
		double sqrN = n*n;
		//denomenator
		double denom = n * (sqrN-1.0);
		//final rho
		return 1-(numer/denom);
	}
	
	public RankedFloatArray rank(float[] f){
		RankSample[] rs = new RankSample[f.length];
		for (int i=0; i< f.length; i++) {
			rs[i] = new RankSample(i, f[i]);
		}
		//sort by value
		Arrays.sort(rs, valueComp);
		//rank
		return rankSamples(rs);
	}
	
	/**Ranks a sorted array of RankSample based on value.
	 * If no ties are found then this is simply their array index number+1.
	 * (ie 1,2,3,4...)
	 * If ties are encountered, ties are assigned the average of their index
	 * positions+1. (ie if index+1's: 2,3,4 have the same absolute difference, all are assigned a
	 * rank of 3).
	 */
	public RankedFloatArray rankSamples(RankSample[] rs){
		int num = rs.length;
		boolean tiesFound = false;
		//assign ranks as index+1
		for (int i=0; i<num; i++) {
			rs[i].rank=i+1;
		}
		//check for ties
		int start;
		int end;
		for (int i=0; i<num; i++){
			start = i;
			end = i;
			//advance stop until the former and latter don't have the same value and the stop
			//	of the array hasn't been reached
			while (++end < num && rs[start].value==rs[end].value){}
			//check if i was advanced
			if (end-start!=1){// ties found
				tiesFound = true;
				//get average of ranks		
				float ave = Num.getAverageInts((int)rs[start].rank, (int)rs[end-1].rank);
				//assign averages
				for (int x=start; x<end; x++) rs[x].rank = ave;
				//reset i
				i=end-1;
			}		
		}
		
		//sort by original position
		Arrays.sort(rs, indexComp);
		//make float[] of ranks
		float[] ranks = new float[rs.length];
		for (int i=0; i< rs.length; i++) {
			ranks[i] = rs[i].rank;
			//System.out.println("Fin "+rs[i].index+" "+rs[i].value+" "+rs[i].rank+" "+tiesFound);			
		}
		rs = null;
		return new RankedFloatArray(tiesFound, ranks);

	}	
	
	//for testing
	public static void main(String[] argv) throws Exception {
		
		
		BufferedReader a_file = new BufferedReader(new FileReader(argv[0]));
		int lines = 0;
		String line = a_file.readLine();
		while (line!=null) {line = a_file.readLine();lines++;}
		lines -= 1;
		a_file.close();

		//System.out.println(" num of lines : " + lines);
		
		float[] a = new float[lines], b = new float[lines];

		a_file = new BufferedReader(new FileReader(argv[0]));
		BufferedReader b_file = new BufferedReader(new FileReader(argv[1]));

		String a_line = a_file.readLine();a_line = a_file.readLine();
		String b_line = b_file.readLine();b_line = b_file.readLine();
		lines = 0;
	
		//Vector <Triple <String>> new_vector = new Vector <Triple <String>> (); //PROVA
		
		while (a_line!=null) {
			
			a[lines] = new Float(a_line.split("\t")[5]);
			b[lines] = new Float(b_line.split("\t")[5]);
			//out_for_statistical_significance.write(a_line.split("\t")[5] + " " + b_line.split("\t")[5] + "\n");
			//new_vector.add(new Triple<String>(a_line.split("\t")[5],b_line.split("\t")[5],""));
			lines ++;
			a_line = a_file.readLine(); b_line = b_file.readLine();
		}
		a_file.close();
		b_file.close();
		
		SpearmanCorrelation sp = new SpearmanCorrelation();
		System.out.print(sp.spearmanCorrelationCoefficient(a, b));

		
		a = normalize(a);
		b = normalize(b);
		BufferedWriter out_for_statistical_significance = new BufferedWriter(new FileWriter(argv[0]+"_4statsig"));
		for (int i = 0; i < a.length;i++) {
			out_for_statistical_significance.write(""+ a[i] + " " + b[i] + "\n");
		}
		out_for_statistical_significance.close();

		//System.out.print("\n"+sp.spearmanCorrelationCoefficient(a, b));

		
		/// ANALISI BISLACCA
		
	    // ApproximateRandomizationTest<Triple<String>> art = new SpearmanCorrelation4StatSigART();
	    //Vector <Triple <String>> new_vector = art.readInput(argv[0]+"_4statsig");
	    
	    //SpearmanCorrelation4StatSig a_new_vector = new SpearmanCorrelation4StatSig();
	    //for (Triple <String> t:new_vector) a_new_vector.add_observation(t);
	    
	    //System.out.println( " new :" + a_new_vector.compute());
	    
		
		/// --------
		
		
	}
	
	private static float [] normalize(float [] a ) {
		float [] a_new = new float [a.length];
		float max = 0, min = 1;
		for (int i = 0; i < a.length; i++) {
			if (a[i]>max) max=a[i];
			if (a[i]<min) min=a[i];
		}
		for (int i = 0; i < a.length; i++) {
			a_new[i] = (a[i] - min)/(max-min);
		}
		return a_new;
		
	}

	public RankedFloatArray getARFA() {
		return aRFA;
	}

	public void setARFA(RankedFloatArray arfa) {
		aRFA = arfa;
	}

	public RankedFloatArray getBRFA() {
		return bRFA;
	}

	public void setBRFA(RankedFloatArray brfa) {
		bRFA = brfa;
	}

}
