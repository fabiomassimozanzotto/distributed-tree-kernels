package it.uniroma2.util.math;

import java.io.*;

public class ArrayMath {


	
	public static double cosine(double [] a,double [] b) throws Exception {
		double cosine = 0, norm_a = 0, norm_b = 0;
		int size = a.length;
		for (int i=0; i<size; i++) { cosine += a[i]*b[i]; norm_a += a[i]*a[i]; norm_b += b[i]*b[i];}
		return cosine/Math.sqrt(norm_a*norm_b);
	}

	public static double dot(double [] a,double [] b) throws Exception {
		double cosine = 0;
		int size = a.length;
		for (int i=0; i<size; i++) { cosine += a[i]*b[i];}
		return cosine;
	}

	public static double [] sum(double [] a,double [] b) throws Exception {
		int size = a.length;
		double [] c = new double [size];
		for (int i=0; i<size; i++) { c[i] = a[i] + b[i];}
		return c;
	}

	public static double norm(double [] a) throws Exception {
		double n = 0;
		for (int i=0;i<a.length;i++) n+= a[i]*a[i];
		return Math.sqrt(n);
	}

	public static float norm(float [] a) throws Exception {
		float n = 0;
		for (int i=0;i<a.length;i++) n+= a[i]*a[i];
		return (float) Math.sqrt(n);
	}	
	
	
	public static double [] versor(double [] a) throws Exception {
		double [] out = new double[a.length];
		double n = ArrayMath.norm(a);
		//System.out.println("NORMA " + n);
		if (n != 0) for (int i=0; i<out.length;i++) out[i] = a[i]/n;
		return out; 
	}

	public static float [] versor(float [] a) throws Exception {
		float [] out = new float[a.length];
		float n = ArrayMath.norm(a);
		//System.out.println("NORMA " + n);
		if (n != 0) for (int i=0; i<out.length;i++) out[i] = a[i]/n;
		return out; 
	}	
	
	public static double [] scalardot(double scalar, double [] a) {
		double [] out = new double[a.length];
		//System.out.println("NORMA " + n);
		for (int i=0; i<out.length;i++) out[i] = scalar*a[i];
		return out; 
	}

	public static Float [] scalardot(Float scalar, Float [] a) throws Exception {
		Float [] out = new Float[a.length];
		//System.out.println("NORMA " + n);
		for (int i=0; i<out.length;i++) out[i] = scalar*a[i];
		return out; 
	}
	
	public static double [] arrayReader(String line,int size) throws Exception {
		double [] array = null; 
		if (line != null) {
			//System.out.println(line);
			array = new double[size];
			if (line.split("\t").length > 1) {
				String []  a = line.split("\t")[1].split(" ");
				for (String s:a) {
					array[new Integer(s.split(":")[0])-1] = new Double(s.split(":")[0]); 
				}
			}
		}
		return array;
	}

	public static String arrayToString(double []  a) throws Exception {
		String out ="";
		for (int i=0;i < a.length;i++) {
			out += " " + a[i];
		}
		return out.trim();
	}
	
	public static Float [] convertToFloatArray(double [] v) {
		Float [] f = new Float[v.length];
		for (int i=0;i < v.length;i++) f[i] = new Float(v[i]);
		return f;
	}

	public static float [] convertTofloatArray(double [] v) {
		float [] f = new float[v.length];
		for (int i=0;i < v.length;i++) f[i] = new Float(v[i]);
		return f;
	}

		
	public static void main(String [] argv) throws Exception {
		BufferedReader a = new BufferedReader(new FileReader(argv[0]));
		BufferedReader b = new BufferedReader(new FileReader(argv[1]));
		
		String dimensions = a.readLine();
		b.readLine();
		String line_a =  a.readLine();
		String line_b =  b.readLine();
		double [] a_array = null, b_array = null;
		int no_of_feats = new Integer(dimensions.split(" ")[1]);
		while (line_a != null && line_b != null) {
			a_array = arrayReader(line_a,no_of_feats);
			b_array = arrayReader(line_b,no_of_feats);
			System.out.println("" + cosine(a_array,b_array));
			line_a =  a.readLine();
			line_b =  b.readLine();
		}
		a.close(); b.close();
	}

	public static String arrayToString(Float[] a) {
		String out ="";
		for (int i=0;i < a.length;i++) {
			out += " " + a[i];
		}
		return out.trim();
	}

	
	public static double [] convertToDoubleArray(Float [] a) {
		double [] aa = new double [a.length];
		for (int i=0; i< a.length; i++) aa[i] = a[i];
		return aa;
		
	}
	
	public static double[] copyDispalced(double[] out_vect, double[] vectorToBeCopied, int displacement) {
		for (int i = displacement; i < displacement + vectorToBeCopied.length ; i++) {
			out_vect[i] = (float) vectorToBeCopied[i-displacement];
		}
		return out_vect;
	}

}
