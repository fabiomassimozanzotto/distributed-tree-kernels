package it.uniroma2.dsk;

/**
 * @author Lorenzo Dell'Arciprete
 * 
 * Basic interface for Distributed Sequences framework
 *
 */
public interface DS<T> {

	/**
	 * @param s - a sequence
	 * @return the Distributed Sequence representation for sequence s
	 */
	public double[] ds(T[] s);
	
	/**
	 * @param s - a (sub)sequence; null elements represent gaps 
	 * @return the Distributed Sequence Fragment representation for (sub)sequence s
	 * @throws Exception 
	 */
	public double[] dsf(T[] s) throws Exception;
	
}
