package it.uniroma2.dsk;

import it.uniroma2.dtk.op.IdealOperation;

/**
 * @author Lorenzo Dell'Arciprete
 * Default implementation of a p-bounded DS.
 */
public abstract class BoundedAbstractDS<T> extends DefaultAbstractDS<T> {
	
	protected int p;

	public int getBound() {
		return p;
	}

	public BoundedAbstractDS(int randomOffset, int vectorsSize, double lambda, int bound, Class<?> opImplementationClass) throws Exception {
		super(randomOffset, vectorsSize, lambda, opImplementationClass);
		p = bound;
	}
	
	public BoundedAbstractDS(int randomOffset, int vectorsSize, double lambda, int bound, IdealOperation opImplementation) throws Exception {
		super(randomOffset, vectorsSize, lambda, opImplementation);
		p = bound;
	}
}
