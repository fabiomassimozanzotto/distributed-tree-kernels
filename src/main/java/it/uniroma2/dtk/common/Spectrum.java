package it.uniroma2.dtk.common;

import java.util.Vector;

/**
 * @author Fabio Massimo Zanzotto
 * A class for a generic vector representing a sum of individual vectors. 
 * It allows for optionally keeping track of the individual vector and the trees they represent.
 */
public class Spectrum {
	
	private double [] vector = null;
	private Vector <double []> vectors = new Vector <double []> ();
	private Vector <String> trees = new Vector<String>();
	private Vector <Integer> trees_hash_values = new Vector<Integer>();
	private double depth = 0;
	
	public double[] getVector() {
		return vector;
	}
	public void setVector(double[] vector) {
		this.vector = vector;
	}
	public Vector<double[]> getVectors() {
		return vectors;
	}
	public void setVectors(Vector<double[]> vectors) {
		this.vectors = vectors;
	}
	public Vector<String> getTrees() {
		return trees;
	}
	public void setTrees(Vector<String> trees) {
		this.trees = trees;
	}
	public double getDepth() {
		return depth;
	}
	public void setDepth(double depth) {
		this.depth = depth;
	}
	public void setTreesHashValues(Vector <Integer> trees_hash_values) {
		this.trees_hash_values = trees_hash_values;
	}
	public Vector <Integer> getTreesHashValues() {
		return trees_hash_values;
	}
}