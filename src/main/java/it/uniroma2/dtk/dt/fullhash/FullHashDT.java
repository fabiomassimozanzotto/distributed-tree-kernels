package it.uniroma2.dtk.dt.fullhash;

import java.util.Map;

import it.uniroma2.dtk.dt.DT;
import it.uniroma2.util.tree.Tree;
import it.uniroma2.util.vector.RandomVectorGenerator;

public class FullHashDT implements DT {
	
	private RandomVectorGenerator rvg;
	
	public FullHashDT(int randomSeed, int vectorSize) {
		rvg = new RandomVectorGenerator(vectorSize, randomSeed);
	}

	public double[] dtf(Tree x) {
		return rvg.getVector(x.toPennTree());
	}

	public double[] dt(Tree x) {
		System.err.println("Not implemented (yet?)");
		return null;
	}
	public double[] dt(Tree x, Map<Tree,double[]> delta) {
		System.out.println("ERROR: THIS IS STILL NOT IMPLEMENTED");
		return null;
	}

}
