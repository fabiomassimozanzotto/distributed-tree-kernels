package it.uniroma2.dtk.dt;

import it.uniroma2.dtk.op.IdealOperation;

/**
 * @author Lorenzo Dell'Arciprete, Fabio Massimo Zanzotto
 * Realization of the DefaultAbstractDT, providing the possibility to plug-in the class
 * implementing the vector composition function to be used.
 */
public class GenericDT extends DefaultAbstractDT {

	private IdealOperation op;
	
	public GenericDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, double lambda, IdealOperation opImplementation) throws Exception {
		super(randomOffset, vectorsSize, usePos, lexicalized, lambda);
		op = opImplementation;
	}
	
	public GenericDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, double lambda, Class<?> opImplementationClass) throws Exception {
		super(randomOffset, vectorsSize, usePos, lexicalized, lambda);
		if (!IdealOperation.class.isAssignableFrom(opImplementationClass))
			throw new Exception("Class "+opImplementationClass+" does not implement interface IdealOperation!");
		op = (IdealOperation) opImplementationClass.newInstance();
		op.initialize(vectorProvider);
	}

	/**
	 * Build a distributed tree and distributed tree fragment generator, with the specified parameters.
	 * The class implementing the vector composition operation must implement interface IdealOperation
	 * and provide a null argument constructor. 
	 * After instantiation, method IdealOperation.initialize(VectorProvider) is invoked.  
	 * 
	 * @param randomOffset: the seed of the RandomVectorGenerator
	 * @param vectorsSize: the size of the vector space
	 * @param lambda: the value for the lambda decaying factor
	 * @param opImplementationClass: the class implementing the vector composition operation
	 * @throws Exception
	 */
	public GenericDT(int randomOffset, int vectorsSize, double lambda, Class<?> opImplementationClass) throws Exception {
		this(randomOffset, vectorsSize, false, true, lambda, opImplementationClass);
	}
	
	/**
	 * Build a distributed tree and distributed tree fragment generator, with the specified parameters.
	 * 
	 * @param randomOffset: the seed of the RandomVectorGenerator
	 * @param vectorsSize: the size of the vector space
	 * @param lambda: the value for the lambda decaying factor
	 * @param opImplementation: the implementation of the vector composition operation
	 * @throws Exception
	 */
	public GenericDT(int randomOffset, int vectorsSize, double lambda, IdealOperation opImplementation) throws Exception {
		this(randomOffset, vectorsSize, false, true, lambda, opImplementation);
	}

	public GenericDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, Class<?> opImplementationClass) throws Exception {
		this(randomOffset, vectorsSize, usePos, lexicalized, 1, opImplementationClass);
	}
	
	public GenericDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, IdealOperation opImplementation) throws Exception {
		this(randomOffset, vectorsSize, usePos, lexicalized, 1, opImplementation);
	}
	
	@Override
	public double[] op(double[] v1, double[] v2) throws Exception {
		if (op == null)
			throw new Exception("Composition function class not initialized!");
		return op.op(v1, v2);
	}
	
	public Class<?> getIdealOperation() {
		return op.getClass();
	}

}
