package it.uniroma2.dtk.dt.partial;

import it.uniroma2.dtk.op.IdealOperation;

/**
 * @author Lorenzo Dell'Arciprete, Fabio Massimo Zanzotto
 * Realization of the AbstractPartialDT, providing the possibility to plug-in the class
 * implementing the vector composition function to be used.
 */
public class GenericPartialDT extends AbstractPartialDT {

	private IdealOperation op;
	
	public GenericPartialDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, double lambda, double mu, IdealOperation opImplementation) throws Exception {
		super(randomOffset, vectorsSize, usePos, lexicalized, lambda, mu);
		op = opImplementation;
	}
	
	public GenericPartialDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, double lambda, double mu, Class<?> opImplementationClass) throws Exception {
		super(randomOffset, vectorsSize, usePos, lexicalized, lambda, mu);
		if (!IdealOperation.class.isAssignableFrom(opImplementationClass))
			throw new Exception("Class "+opImplementationClass+" does not implement interface IdealOperation!");
		op = (IdealOperation) opImplementationClass.newInstance();
		op.initialize(vectorProvider);
	}
	public GenericPartialDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, double lambda, double mu, double terminalFactor, IdealOperation opImplementation) throws Exception {
		super(randomOffset, vectorsSize, usePos, lexicalized, lambda, mu, terminalFactor);
		op = opImplementation;
	}
	
	public GenericPartialDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, double lambda, double mu, double terminalFactor, Class<?> opImplementationClass) throws Exception {
		super(randomOffset, vectorsSize, usePos, lexicalized, lambda, mu, terminalFactor);
		if (!IdealOperation.class.isAssignableFrom(opImplementationClass))
			throw new Exception("Class "+opImplementationClass+" does not implement interface IdealOperation!");
		op = (IdealOperation) opImplementationClass.newInstance();
		op.initialize(vectorProvider);
	}
	public GenericPartialDT(int randomOffset, int vectorsSize, double lambda, double mu, double terminalFactor, Class<?> opImplementationClass) throws Exception {
		this(randomOffset, vectorsSize, false, true, lambda, mu, terminalFactor, opImplementationClass);
	}

	/**
	 * Build a distributed tree generator, with the specified parameters.
	 * The class implementing the vector composition operation must implement interface IdealOperation
	 * and provide a null argument constructor. 
	 * After instantiation, method IdealOperation.initialize(VectorProvider) is invoked.  
	 * 
	 * @param randomOffset: the seed of the RandomVectorGenerator
	 * @param vectorsSize: the size of the vector space
	 * @param lambda: the value for the lambda decaying factor (see Tree Kernel)
	 * @param mu: the value for the mu decaying factor (see String Kernel)
	 * @param opImplementationClass: the class implementing the vector composition operation
	 * @throws Exception
	 */
	public GenericPartialDT(int randomOffset, int vectorsSize, double lambda, double mu, Class<?> opImplementationClass) throws Exception {
		this(randomOffset, vectorsSize, false, true, lambda, mu, opImplementationClass);
	}
	
	/**
	 * Build a distributed tree generator, with the specified parameters.
	 * 
	 * @param randomOffset: the seed of the RandomVectorGenerator
	 * @param vectorsSize: the size of the vector space
	 * @param lambda: the value for the lambda decaying factor
	 * @param opImplementation: the implementation of the vector composition operation
	 * @throws Exception
	 */
	public GenericPartialDT(int randomOffset, int vectorsSize, double lambda, double mu, IdealOperation opImplementation) throws Exception {
		this(randomOffset, vectorsSize, false, true, lambda, mu, opImplementation);
	}

	public GenericPartialDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, Class<?> opImplementationClass) throws Exception {
		this(randomOffset, vectorsSize, usePos, lexicalized, 1, 1, opImplementationClass);
	}
	
	public GenericPartialDT(int randomOffset, int vectorsSize, boolean usePos, boolean lexicalized, IdealOperation opImplementation) throws Exception {
		this(randomOffset, vectorsSize, usePos, lexicalized, 1, 1, opImplementation);
	}
	
	@Override
	public double[] op(double[] v1, double[] v2) throws Exception {
		if (op == null)
			throw new Exception("Composition function class not initialized!");
		return op.op(v1, v2);
	}
	
	public Class<?> getIdealOperation() {
		return op.getClass();
	}

}
