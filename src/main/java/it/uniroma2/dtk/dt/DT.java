package it.uniroma2.dtk.dt;

import java.util.Map;

import it.uniroma2.util.tree.Tree;

/**
 * @author Fabio Massimo Zanzotto
 * 
 * Basic interface for Distributed Trees framework
 *
 */
public interface DT {
	/**
	 * @param x - a tree
	 * @return the Distributed Tree Fragment representation for tree x
	 */
	public double[] dtf(Tree x);
	
	/**
	 * @param x - a tree
	 * @return the Distributed Tree representation for tree x
	 */
	public double[] dt(Tree x);

	/**
	 * @param x - a tree
	 * @return the Distributed Tree representation for tree x
	 */
	public double[] dt(Tree x, Map<Tree,double[]> delta);

}
