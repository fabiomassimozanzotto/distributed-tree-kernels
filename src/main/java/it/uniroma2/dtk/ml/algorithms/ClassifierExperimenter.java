package it.uniroma2.dtk.ml.algorithms;

import java.io.File;

public class ClassifierExperimenter {
	
	
	public static void main(String [] argv) throws Exception {
		GenericLearner gl = new Pegasos();
		
		((Pegasos)gl).setLambda((float) 0.001);
		
		gl.learn(new File("C:\\USERS_DATA\\FABIO\\LAVORO\\temp\\CUDA_DTK\\RTE2_dev_processed.xml.svm"), 
				 new File("C:\\USERS_DATA\\FABIO\\LAVORO\\temp\\CUDA_DTK\\RTE2_dev_processed.model"));
//		gl.learn(new File("C:\\USERS_DATA\\FABIO\\LAVORO\\temp\\CUDA_DTK\\prova.svm"), 
//				 new File("C:\\USERS_DATA\\FABIO\\LAVORO\\temp\\CUDA_DTK\\RTE2_dev_processed.model"));

		Classifier c = new Classifier(gl.getModel(), gl.getModelProperties());
		c.classify(new File("C:\\USERS_DATA\\FABIO\\LAVORO\\temp\\CUDA_DTK\\RTE2_test_processed.xml.svm"), 
				 new File("C:\\USERS_DATA\\FABIO\\LAVORO\\temp\\CUDA_DTK\\RTE2_test_processed.out"));
		
	
	}

}
