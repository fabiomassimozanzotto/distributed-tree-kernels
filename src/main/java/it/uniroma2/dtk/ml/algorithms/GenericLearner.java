package it.uniroma2.dtk.ml.algorithms;

import it.uniroma2.dtk.dt.GenericDT;
import it.uniroma2.dtk.ml.datastructs.DenseInstance;
import it.uniroma2.dtk.op.IdealOperation;
import it.uniroma2.dtk.op.convolution.ShuffledCircularConvolution;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;

public abstract class GenericLearner {
	private boolean shuffleTrainingData = true;
	private int explicitSpaceDimension = 1 ;
	private DenseMatrix64F model = null;
	private int displaystep = 100;
	private GenericDT dt_generator;
	private Properties modelProperties = new Properties();
	

	public DenseMatrix64F getModel() {
		return model;
	}
	
	public void setModel(DenseMatrix64F model) {
		this.model = model;
	}
	
	
	public Properties getModelProperties() {
		return modelProperties;
	}

	public void setModelProperties(Properties modelProperties) {
		this.modelProperties = modelProperties;
	}

	public abstract void updateModel(DenseInstance instance, int iteration);

	public void learn(File training, File modelFile) throws Exception {
		modelProperties = new Properties();
		modelProperties.setProperty("randomOffset","0");
		modelProperties.setProperty("vectorsSize","4096");
		modelProperties.setProperty("usePos","false");
		modelProperties.setProperty("lexicalized","true");
		modelProperties.setProperty("lambda","0.4");
		modelProperties.setProperty("relevantTrees", "2,3");
		modelProperties.setProperty("explicitSpaceDimension","1");
		
		System.out.println("Learning parameters: " + modelProperties);
		
		dt_generator = new GenericDT(new Integer(modelProperties.getProperty("randomOffset")), new Integer(modelProperties.getProperty("vectorsSize")), 
				new Boolean(modelProperties.getProperty("usePos")), new Boolean(modelProperties.getProperty("lexicalized")),
				new Double(modelProperties.getProperty("lambda")), (new ShuffledCircularConvolution()).getClass());
		
		if (shuffleTrainingData) {
			System.out.println("Shuffling training data");
			shuffleFile(training,new File(training.getAbsolutePath() + ".shuff"),new Random(8));
			training = new File(training.getAbsolutePath() + ".shuff");
		}
		
		model = new DenseMatrix64F(1, (explicitSpaceDimension==1?2:explicitSpaceDimension) + modelProperties.getProperty("relevantTrees").split(",").length*dt_generator.getVectorSize() );
//		model = new DenseMatrix64F(1, 2);
		model.zero();
		
		System.out.print("Start learning ");
		BufferedReader input = new BufferedReader(new FileReader(training));
		String inputLine;
		int processed_examples = 0;
		while ((inputLine = input.readLine())!= null) {
			processed_examples++;
			updateModel(DenseInstance.fromString(inputLine,new Integer(modelProperties.getProperty("explicitSpaceDimension")),modelProperties.getProperty("relevantTrees").split(","),dt_generator),processed_examples);
			if (processed_examples%displaystep == 0) {
				System.out.print("..."+processed_examples);
//				System.out.print("\nModel: ");
//				for (int j=0;j<2;j++) System.out.print(" " + model.get(0, j));
			}
		}
		System.out.println("... done (processed "+processed_examples + " training instances)");
		storeModel(modelFile);
	}

	private void shuffleFile(File training, File file, Random r) throws IOException {
		(new LineShuffler()).shuffle(training, file, r);
	}

	private void storeModel(File modelFile) {
        try {
            MatrixIO.saveBin(model,modelFile.getAbsolutePath());
            modelProperties.store(new FileWriter(modelFile.getAbsolutePath()+".info"), "Pegasos-StructIn Learnt Model " + new Date(System.currentTimeMillis()) );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
	}

	
	
}
