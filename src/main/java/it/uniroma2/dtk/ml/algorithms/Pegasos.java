package it.uniroma2.dtk.ml.algorithms;

import java.io.File;
import java.io.IOException;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;
import org.ejml.ops.NormOps;

import it.uniroma2.dtk.ml.datastructs.DenseInstance;

public class Pegasos extends GenericLearner {

	private float lambda = 1;

	public void updateModel(DenseInstance instance, int iteration) {
		DenseMatrix64F c = new DenseMatrix64F(1,1);
		CommonOps.multTransB(getModel(), instance.getVector(), c);
		
		//System.out.println("Norm = " + NormOps.normF(instance.getVector()));
		
		DenseMatrix64F my_model = getModel();
		CommonOps.scale((1 - 1/iteration), my_model);
		//System.out.println("V = " + c.get(0, 0)*instance.theClass());
		setModel(my_model);
		if (c.get(0, 0)*instance.theClass() < 1 ) {
			my_model = getModel();
			//System.out.println("w = " +instance.theClass()/(((double)iteration)*((double)lambda)));
			CommonOps.add(my_model, instance.theClass()/(((double)iteration)*((double)lambda)), instance.getVector(), my_model);
			
			double norm_model = NormOps.normF(my_model);
			double resize_factor = 1/(Math.sqrt(lambda)*norm_model);
			
			if (resize_factor < 1) 	CommonOps.scale(resize_factor, my_model);
			setModel(my_model);

		}
	}

	public float getLambda() {
		return lambda;
	}

	public void setLambda(float lambda) {
		this.lambda = lambda;
	}

	
	public static void main(String [] argv) {
		GenericLearner learner = new Pegasos();
		try {
			learner.learn(new File("Prova"), new File("Prova.model"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
