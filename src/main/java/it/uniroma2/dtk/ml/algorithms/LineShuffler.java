package it.uniroma2.dtk.ml.algorithms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;

public class LineShuffler {
	Random generator = null;
	
	public void shuffle(File in, File out, Random r ) throws IOException {

		generator = r;
		
		BufferedReader input = new BufferedReader(new FileReader(in));
		
		Vector<String> lines = new Vector<String>();
		String line = null;
		while ((line = input.readLine())!=null) lines.add(line);
		input.close();
		
		String [] corpus = lines.toArray(new String[lines.size()]);
		
		shuffle(corpus);
		BufferedWriter output = new BufferedWriter(new FileWriter(out));
//		int positive = 0;
		for (String o:corpus) {
//			if (o.startsWith("+1")) {
				output.write(o+ "\n");
//				positive += 3;
//			} else if (positive>0){
//				output.write(o+ "\n");
//				positive--;
//			}
		}
		output.close();
	}

    public void exch(String[] a, int i, int j) {
        String swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    // take as input an array of strings and rearrange them in random order
    public void shuffle(String[] a) {
        int N = a.length;
        for (int i = 0; i < N; i++) {
            int r = i + (int) (generator.nextFloat() * (N-i));   // between i and N-1
            exch(a, i, r);
        }
    }

    // take as input an array of strings and print them out to standard output
    public void show(String[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
	
}
