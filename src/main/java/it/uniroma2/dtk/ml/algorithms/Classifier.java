package it.uniroma2.dtk.ml.algorithms;

import it.uniroma2.dtk.dt.GenericDT;
import it.uniroma2.dtk.ml.datastructs.DenseInstance;
import it.uniroma2.dtk.ml.evaluation.CollectionOfResults;
import it.uniroma2.dtk.op.convolution.ShuffledCircularConvolution;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;
import org.ejml.ops.MatrixIO;

public class Classifier {
	
	DenseMatrix64F model;
	Properties modelProperties;
	GenericDT dt_generator ;

	Vector<Integer> relevantTrees;
	
	int displaystep = 100;
	
	public Classifier(DenseMatrix64F model, Properties modelProperties) throws NumberFormatException, Exception {
		this.model = model;
		this.modelProperties = modelProperties; 
		dt_generator = new GenericDT(new Integer(modelProperties.getProperty("randomOffset")), new Integer(modelProperties.getProperty("vectorsSize")), 
				new Boolean(modelProperties.getProperty("usePos")), new Boolean(modelProperties.getProperty("lexicalized")),
				new Double(modelProperties.getProperty("lambda")), (new ShuffledCircularConvolution()).getClass());
	}
	
	public Classifier(File modelFile) throws NumberFormatException, Exception {
		this.model = MatrixIO.loadBin(modelFile.getAbsolutePath());
		Properties modelProperties = new Properties();
		modelProperties.load(new FileReader(modelFile.getAbsolutePath()+".info"));
		dt_generator = new GenericDT(new Integer(modelProperties.getProperty("randomOffset")), new Integer(modelProperties.getProperty("vectorsSize")), 
				new Boolean(modelProperties.getProperty("usePos")), new Boolean(modelProperties.getProperty("lexicalized")),
				new Double(modelProperties.getProperty("lambda")), (new ShuffledCircularConvolution()).getClass());

	}
	
	public double classify(DenseInstance i) {
		DenseMatrix64F output = new DenseMatrix64F(1,1);
		CommonOps.multTransB(model, i.getVector(), output);
		return output.get(0, 0);
	}
	
	public void classify(File input, File output) throws Exception {
		BufferedReader in = new BufferedReader(new FileReader(input));
		BufferedWriter out = new BufferedWriter(new FileWriter(output));
		String inputLine;
		int processed_examples = 0;
		CollectionOfResults res = new CollectionOfResults();
		while ((inputLine = in.readLine())!= null) {
			processed_examples++;
			DenseInstance inst = DenseInstance.fromString(inputLine,new Integer(modelProperties.getProperty("explicitSpaceDimension")),modelProperties.getProperty("relevantTrees").split(","),dt_generator);
			double decision = classify(inst);
			out.write(decision+"\n");
			res.add(decision, inst.theClass());
			if (processed_examples%displaystep == 0) System.out.print("..."+processed_examples);
		}
		out.close();
		System.out.println("... done (processed "+processed_examples + " training instances)");
		System.out.println(res.performance());
	}


}
