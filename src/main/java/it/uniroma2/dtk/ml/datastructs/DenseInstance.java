package it.uniroma2.dtk.ml.datastructs;

import it.uniroma2.dtk.dt.GenericDT;
import it.uniroma2.util.tree.Tree;

import java.util.StringTokenizer;
import java.util.Vector;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;
import org.ejml.ops.NormOps;


public class DenseInstance {
	
	DenseMatrix64F vector = null;
	float theClass = 0;
	Vector<Tree> trees = null;
	
	
	public DenseMatrix64F getVector() {
		return vector;
	}

	public void setVector(DenseMatrix64F vector) {
		this.vector = vector;
	}

 
	/** 
     * reads instances in the form : [1|-1] |BT| tree |BT| tree |BT| tree |ET| |BV| a:v a:v |EV|
	 * @throws Exception 
     */
	public static DenseInstance fromString(String inputLine, int explicitSpaceDimension, String [] relevantTrees, GenericDT dt) throws Exception {
		if (relevantTrees==null) throw new Exception("relevantTrees not set");
		if (dt==null) throw new Exception("DistributedTreeGenerator not set");
		DenseInstance instance = new DenseInstance();
     	boolean degenerated_dimension = false;

     	// Setting the class of the instance
     	//System.out.println(">"+inputLine.split("[\\t|\\s]")[0].trim()+"<");
    	instance.theClass = new Float(inputLine.split("[\\t|\\s]")[0].trim());

     	
     	String[] tokens = inputLine.split("\\|ET\\|");

    	if (explicitSpaceDimension == 1)  { degenerated_dimension = true; explicitSpaceDimension = 2;}
    	
    	instance.vector = new DenseMatrix64F(1,explicitSpaceDimension + relevantTrees.length * dt.getVectorSize());
    	
    	instance.vector.zero();
    	
    	if (tokens.length > 1) {
	    	instance.trees = new Vector<Tree>();
	    	String[] string_trees= tokens[0].split("\\|BT\\|");
	    	
	    	
	    	for (int i=1; i < string_trees.length; i++){
	    		Tree t = treeCleaner(Tree.fromPennTree(string_trees[i].trim()));
	    		instance.trees.add(t);
	    	}
    	}

    	// Adding distributed vectors in the final vector
    	for (int i = 0 ; i < relevantTrees.length ; i++) {
    		double [] v = dt.dt(instance.trees.elementAt(new Integer(relevantTrees[i])));
    		DenseMatrix64F v64F = new DenseMatrix64F(1,v.length,true, v);
        	// Normalizing distributed vectors
    		NormOps.normalizeF(v64F);
//        	System.out.println("Norm (v64F): " +  NormOps.normF(v64F));
    		
	    	CommonOps.insert(v64F, instance.vector, 0, explicitSpaceDimension + i*dt.getVectorSize());
//	    	double [] dd  = instance.vector.data;
//	    	for (int kk=0;kk< explicitSpaceDimension + relevantTrees.length*dt.getVectorSize();kk++) {
//	    		System.out.print(" " + dd[kk]);
//	    	}
//	    	System.out.println();
    	}

    	tokens = inputLine.trim().split("\\|EV\\|");

    	if (tokens.length == 1) {
    		
    		DenseMatrix64F explicit_features = new DenseMatrix64F(1,explicitSpaceDimension);
    		
	    	String[] string_vector = tokens[0].split("\\|BV\\|");
	    	StringTokenizer st = new StringTokenizer(string_vector[1]," \t\n\r\f:");
	      	while (st.hasMoreTokens()) {
	      		Integer feature_number = (new Integer(st.nextToken()));
		      	Float value = new Float(st.nextToken());
		      	if (value.isNaN()) value = new Float(0);
	      		explicit_features.add(0, feature_number - 1 , value);
	      	}
    		if (degenerated_dimension) explicit_features.add(0, 1 , 1);

    		// vector normalization
    		NormOps.normalizeF(explicit_features);
    		
//        	System.out.println("Norm (explicit_features): " +  NormOps.normF(explicit_features));

        	CommonOps.insert(explicit_features, instance.vector, 0, 0);
    	}
    	
//    	System.out.println("Norm: " +  NormOps.normF(instance.vector));
//    	double [] dd  = instance.vector.data;
//    	for (int kk=0;kk< explicitSpaceDimension + relevantTrees.length*dt.getVectorSize();kk++) {
//    		System.out.print(" " + dd[kk]);
//    	}
//    	System.out.println();
//    	
//    	System.out.println("Norm: " +  NormOps.normF(instance.vector));
    	NormOps.normalizeF(instance.vector);
		return instance;
	}
	
	public float theClass() {
		return theClass;
	}
	
    private static Tree treeCleaner(Tree t) {
    	if (!t.getRootLabel().equals(":")) t.setRootLabel(t.getRootLabel().split(":")[0]);
    	if (!t.getUsePosLabel().equals(":")) t.setUsePosLabel(t.getUsePosLabel().split(":")[0]);
    	if (! t.isPreTerminal()) 
    		for (Tree tt:t.getChildren()) treeCleaner(tt);
    	return t;
    }

}
