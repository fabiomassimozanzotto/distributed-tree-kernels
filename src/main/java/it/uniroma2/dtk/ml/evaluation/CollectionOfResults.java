package it.uniroma2.dtk.ml.evaluation;

import java.util.Vector;

public class CollectionOfResults {
	
	Vector<Result> results = new Vector<Result>();
	
	public void add(double system, double oracle){
		results.add(new Result(system,oracle));
	}
	
	public Performance performance() {
		double accuracy = 0;
		double positive = 0;
		double true_positive = 0;
		double true_elements = 0;
		for (Result r:results) {
			if (r.oracle*r.system > 0 ) accuracy++;
			if (r.oracle > 0 && r.system > 0 ) true_positive++;
			if (r.system > 0 ) positive++;
			if (r.oracle > 0 ) true_elements++;
		}
		Performance p = new Performance();
		p.accuracy = accuracy/results.size();
		p.precision = true_positive / positive;
		p.recall = true_positive / true_elements;
		p.f_measure = 2*p.precision*p.recall/(p.precision+p.recall);
		return p;
	}
	
	private class Result {
		public Result(double system, double oracle) {
			this.system = system;
			this.oracle = oracle;
		}
		double system;
		double oracle;
	}

}
