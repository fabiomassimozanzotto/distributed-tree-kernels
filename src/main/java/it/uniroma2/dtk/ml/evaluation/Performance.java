package it.uniroma2.dtk.ml.evaluation;

public class Performance {
	
	public double precision, recall, f_measure, accuracy;
	
	
	public String toString() {
		return "Accuracy : " + accuracy +  " : Precision : " + precision + " : Recall: " + recall +  " : f_measure: " + f_measure;
	}

}
