package it.uniroma2.dtk.op.convolution;

import it.uniroma2.util.vector.VectorComposer;

import java.util.Arrays;

public class ShiftedCircularConvolution extends CircularConvolution {
	
	@Override
	public double[] op(double[] x, double[] y) {
		if (x == null)
			return y;
		else if (y == null)
			return x;
		if (fft2 != null) return shiftedConvolutionFFT2(x, y);
		else if (fft != null) return shiftedConvolutionFFT(x, y);
		else return shiftedConvolutionBasic(x, y);
	}
	
	public double[] shiftedConvolutionBasic(double[] firstVector, double[] secondVector) {
		int size = firstVector.length;
		double[] result = new double[size];
		Arrays.fill(result, 0);
		for (int i=0; i<size; i++)
			for (int j=0; j<size; j++) {
				int k = (i-j) < 0 ? (i-j+size) : i-j;
				result[i] += firstVector[j] * secondVector[(k>0) ? k-1 : size-1];
			}
		return result;
	}
	
	public double[] shiftedConvolutionFFT(double[] firstVector, double[] secondVector) {
		return circularConvolutionFFT(VectorComposer.shift(firstVector), VectorComposer.shift(secondVector));
	}
	public double[] shiftedConvolutionFFT2(double[] firstVector, double[] secondVector) {
		return circularConvolutionFFT2(VectorComposer.shift(firstVector), VectorComposer.shift(secondVector));
	}
}
