package it.uniroma2.dtk.op.convolution;

import it.uniroma2.util.vector.VectorComposer;

import java.util.Arrays;

public class ReverseCircularConvolution extends CircularConvolution {
	
	@Override
	public double[] op(double[] x, double[] y) {
		if (x == null)
			return y;
		else if (y == null)
			return x;
		if (fft2 != null) return reverseConvolutionFFT2(x, y);
		else if (fft != null) return reverseConvolutionFFT(x, y);
		else return reverseConvolutionBasic(x, y);
	}
	
	public double[] reverseConvolutionBasic(double[] firstVector, double[] secondVector) {
		int size = firstVector.length;
		double[] result = new double[size];
		Arrays.fill(result, 0);
		for (int i=0; i<size; i++)
			for (int j=0; j<size; j++)
				result[i] += firstVector[j] * secondVector[(i-j)<0 ? size-1-(i-j+size) : size-1-(i-j)];
		return result;
	}
	
	public double[] reverseConvolutionFFT(double[] firstVector, double[] secondVector) {
		return circularConvolutionFFT(VectorComposer.reverse(firstVector), VectorComposer.reverse(secondVector));
	}
	public double[] reverseConvolutionFFT2(double[] firstVector, double[] secondVector) {
		return circularConvolutionFFT2(VectorComposer.reverse(firstVector), VectorComposer.reverse(secondVector));
	}

}
