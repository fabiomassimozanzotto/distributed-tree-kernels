package it.uniroma2.dtk.op.product;

import it.uniroma2.dtk.op.TransformAndCompose;
import it.uniroma2.util.math.ArrayMath;
import it.uniroma2.util.vector.RandomVectorGenerator;
import it.uniroma2.util.vector.VectorProvider;

public class GammaProduct extends TransformAndCompose {

	protected double gamma = 0;	// Gamma parameter for the shuffled gamma-product
	
	@Override
	public double[] op(double[] x, double[] y) {
		return ArrayMath.scalardot(1/gamma, product(x, y));
	}

	public double[] product(double[] firstVector, double[] secondVector) {
		if (firstVector == null)
			return secondVector;
		else if (secondVector == null)
			return firstVector;
		int size = firstVector.length;
		double[] result = new double[size];
		for (int i=0; i<size; i++)
			result[i] = firstVector[i] * secondVector[i];
		return result;
	}
	
	@Override
	public void initialize(VectorProvider vp) throws Exception {
		super.initialize(vp);
		RandomVectorGenerator rvg = null;
		if (vp instanceof RandomVectorGenerator)
			rvg = (RandomVectorGenerator)vp;
		else
			rvg = new RandomVectorGenerator(vp.getVectorSize());
		for (int i=0;i<1000;i++) {
			gamma += ArrayMath.norm(product(rvg.generateRandomVector(), rvg.generateRandomVector()));
		}
		gamma = gamma/1000;
	}
	
}
