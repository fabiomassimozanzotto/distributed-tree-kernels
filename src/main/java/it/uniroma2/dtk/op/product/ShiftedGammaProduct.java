package it.uniroma2.dtk.op.product;

import it.uniroma2.util.math.ArrayMath;

public class ShiftedGammaProduct extends GammaProduct {

	@Override
	public double[] op(double[] x, double[] y) {
		return ArrayMath.scalardot(1/gamma, shiftedProduct(x, y));
	}

	public double[] shiftedProduct(double[] firstVector, double[] secondVector) {
		if (firstVector == null)
			return secondVector;
		else if (secondVector == null)
			return firstVector;
		int size = firstVector.length;
		double[] result = new double[size];
		result[0] = firstVector[0] * secondVector[size-1];
		for (int i=1; i<size; i++)
			result[i] = firstVector[i] * secondVector[i-1];
		return result;
	}
	
}
