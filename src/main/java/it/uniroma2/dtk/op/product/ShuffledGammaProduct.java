package it.uniroma2.dtk.op.product;

import it.uniroma2.util.math.ArrayMath;

public class ShuffledGammaProduct extends GammaProduct {

	public double[] shuffledProduct(double[] firstVector, double[] secondVector) {
		if (firstVector == null)
			return secondVector;
		else if (secondVector == null)
			return firstVector;
		int size = firstVector.length;
		double[] result = new double[size];
		for (int i=0; i<size; i++)
			if (permutation1[i] >= 0) {
				if (permutation2[i] >= 0)
					result[i] = firstVector[permutation1[i]] * secondVector[permutation2[i]];
				else
					result[i] = firstVector[permutation1[i]] * -secondVector[-permutation2[i]];
			}
			else {
				if (permutation2[i] >= 0)
					result[i] = -firstVector[-permutation1[i]] * secondVector[permutation2[i]];
				else
					result[i] = -firstVector[-permutation1[i]] * -secondVector[-permutation2[i]];
			}
		return result;
	}
	
	@Override
	public double[] op(double[] x, double[] y) {
		return ArrayMath.scalardot(1/gamma, shuffledProduct(x, y));
	}
}
