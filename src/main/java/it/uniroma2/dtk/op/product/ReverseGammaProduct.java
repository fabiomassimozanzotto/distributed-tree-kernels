package it.uniroma2.dtk.op.product;

import it.uniroma2.util.math.ArrayMath;

public class ReverseGammaProduct extends GammaProduct {

	@Override
	public double[] op(double[] x, double[] y) {
		return ArrayMath.scalardot(1/gamma, reverseProduct(x, y));
	}
	
	public double[] reverseProduct(double[] firstVector, double[] secondVector) {
		if (firstVector == null)
			return secondVector;
		else if (secondVector == null)
			return firstVector;
		int size = firstVector.length;
		double[] result = new double[size];
		for (int i=0; i<size; i++)
			result[i] = firstVector[i] * secondVector[size - 1 - i];
		return result;
	}

}
