package it.uniroma2.dtk.op;

import it.uniroma2.util.vector.VectorProvider;

/**
 * @author Lorenzo Dell'Arciprete
 * Interface for classes containing an implementation of the ideal vector composition operation.
 * GenericDT allows for the plugging in of any class implementing IdealOperation, either by receiving 
 * an instance or by receiving the Class object. In the latter case, the generic instantiation procedure 
 * requires a null argument constructor, then invokes the initialize method.
 */
public interface IdealOperation {
	
	/**
	 * The actual composition operation
	 * @return the composition of vectors x and y
	 */
	public double[] op(double[] x, double[] y);
	
	/**
	 * Performs required initializations, using the provided VectorProvider where needed 
	 * @param vp - the VectorProvider that will be used for getting the vectors to be composed
	 */
	public void initialize(VectorProvider vp) throws Exception;

}
